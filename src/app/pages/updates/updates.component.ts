import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SanitizedUpdate } from 'src/app/models/update.model';

import { UpdatesService } from '../../services/updates/updates.service';

@Component({
  selector: 'app-updates',
  templateUrl: './updates.component.html',
  styleUrls: ['./updates.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UpdatesComponent implements OnInit {

  updates: SanitizedUpdate[];

  constructor(private updatesService: UpdatesService) { }

  ngOnInit(): void {
    this.updates = this.updatesService.getUpdates();
  }

}
