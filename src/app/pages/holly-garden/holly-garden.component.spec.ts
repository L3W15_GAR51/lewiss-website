import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HollyGardenComponent } from './holly-garden.component';

describe('HollyGardenComponent', () => {
  let component: HollyGardenComponent;
  let fixture: ComponentFixture<HollyGardenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HollyGardenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HollyGardenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
