import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AboutComponent } from "./pages/about/about.component";
import { SalesComponent } from "./pages/sales/sales.component";
import { HomeComponent } from "./pages/home/home.component";
import { CommiteeComponent } from './pages/commitee/commitee.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { UpdatesComponent } from './pages/updates/updates.component';
import { BeehivePageComponent } from "./pages/beehive-page/beehive-page.component";
import { HollyGardenComponent } from "./pages/holly-garden/holly-garden.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
  },
  {
    path: "about",
    component: AboutComponent,
  },
  {
    path: "sales",
    component: SalesComponent,
  },
  {
    path: "committee",
    component: CommiteeComponent,
  },
  {
    path: "gallery",
    component: GalleryComponent
  },
  {
    path: "updates",
    component: UpdatesComponent
  },
  {
    path: "hive",
    component: BeehivePageComponent
  },
  {
    path: "holly-garden",
    component: HollyGardenComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
