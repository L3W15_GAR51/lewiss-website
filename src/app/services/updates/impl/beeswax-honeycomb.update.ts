import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class BeeswaxHoneycomb implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "beeswax-honeycomb";
    }

    getTitle(): string {
        return "Our new endeavour: beeswax furniture polish";
    }

    getDate(): string {
        return "21st October 2020";
    }

    getHtmlContent(): SafeHtml {
        // Overwrite default 1080p styling - filmed in vertical?
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <p>
            We've been wondering what to do with our beeswax. After putting our thoughts together, we decided
            to begin production of Beeswax Furniture Polish. Watch our video below explaining how it's done!
        </p>

        <p class="disclaimer">
            As of February 2021, Beeswax Polish is no longer available for sale.
        </p>
    
        <iframe class="ver" src="https://player.vimeo.com/video/470722858" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        `);
    }

}