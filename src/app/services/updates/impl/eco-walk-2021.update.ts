import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class EcoWalk2021 implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "eco-walk-2021";
    }

    getTitle(): string {
        return "Mr. Harding's 2021 Eco Walk";
    }

    getDate(): string {
        return "4th March 2021";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <p>
            Our Community Co-ordinator at Cardinal Allen has explained how, in 2021, we use our school grounds to promote eco-friendly practices and keep them bee-friendly.
        </p>
    
        <iframe width="560" height="315" class="hor" src="https://www.youtube.com/embed/CWuCT-rG47s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        `);
    }

}