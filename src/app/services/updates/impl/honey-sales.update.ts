import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class HoneySales implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "honey-sales";
    }

    getTitle(): string {
        return "New Honey Sale";
    }

    getDate(): string {
        return "25th May 2023";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`

        <p>
        The Honey Jar Prices are now £7. From the Finance office, Will be ready by June 
        <p> 

    
        `);
    }

}