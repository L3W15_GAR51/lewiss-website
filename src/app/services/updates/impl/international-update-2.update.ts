import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class InternationalUpdate2 implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "international-update-2";
    }

    getTitle(): string {
        return "The Sudesha School: planting bee-friendly plants";
    }

    getDate(): string {
        return "29th January 2021";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <style>
            .images img {
                
            }
        </style>

        <p>
            <a class="nice_link" href="https://sudesha.edu.np">The Sudesha School</a> in Lalitpur, Nepal have been creating bee-friendly grounds on their
            school site as part of CardyHoney. Well done and thank you!
        </p>

        <div class="images">
            <img src="https://i.imgur.com/21jsGDy.jpg" width=240 height=320>
            <img src="https://i.imgur.com/dTi5WC5.jpg" width=240 height=320>
            <img src="https://i.imgur.com/ZVcSBpA.jpg" width=240 height=320>
            <img src="https://i.imgur.com/jHSc6y7.jpg" width=224 height=168>
            <img src="https://i.imgur.com/Vmd8UEl.jpg" width=224 height=168>
            <img src="https://i.imgur.com/U4yqNXK.jpg" width=224 height=168>
        </div>

        `);
    }

}