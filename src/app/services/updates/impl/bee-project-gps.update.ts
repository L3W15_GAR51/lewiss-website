import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class BeeProjectGalaxy implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "bee-project-gps";
    }

    getTitle(): string {
        return "Galaxy Public School's Bee Project";
    }

    getDate(): string {
        return "3rd March 2021";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <p>
            Watch this video, produced by TES and Galaxy Public School, to learn about their bee initiatives as part of CardyHoney!
        </p>
    
        <iframe width="560" height="315" class="hor" src="https://www.youtube.com/embed/wfj-p0zZiEA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        `);
    }

}